## Welcome

On this website, you can find :
 * my research papers
 * some of my personal projects
 * informations about me

Head over to this [GitLab page](https://gitlab.in2p3.fr/jules-colas) to look at my data processing scripts.
