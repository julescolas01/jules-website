---
title: Resume
date: 2015-01-05
---

***
### Contacts
- e-mail **jules.colas@lilo.org**
- téléphone **+33659167001**
- github **https://github.com/j-colas**
- gitlab **X**
- linkedin **X**

***
### Formation

#### **Ecole Normale Supérieure de Lyon** , Lyon, Rhône, France
* Master 2 : **Modélisation des Systèmes Complexes**, _diplômé en septembre 2020_
* Master 1 : **Sciences de la Matière**,  _Sep 2017 – Août 2018_
* Licence 3 : **Sciences de la Matière**, _Sep 2016 – Aout 2017_

#### **Ecole Centrale de Lyon**, Ecully, Rhône, France
 * **Ingénieur généraliste**, _diplômé en septembre 2020_

#### **Université Lyon 1**, Villeurbanne, Rhône, France
* DUT : **Génie Électrique et Informatique Industrielle**, _diplômé en septembre 2016_

***
### Expériences

#### **Institut de Physique des 2 Infinis de Lyon**
* **Stagiaire**, groupe MANOIR, _Mar 2020 – Sep 2020_
* Minimisation des pertes de phonons dans un détecteur à inductance cinétique en configuration ”sans contact”.
* Collaboration avec l’équipe HELFA de l’Institut Néel (Grenoble).
* **Contact**, Dr Alexandre Juillard

#### **FEMTOprint** , Muzzano, Switzerland
* **Stagiaire**, division Solutions, _Avr 2019 – Sep 2019_
* Optimisation des paramètres de gravure laser assistée par dissolution chimique. (confidentiel)

#### **CNRS/Institut de Physique Nucléaire de Lyon**
* **Vacataire**, groupe MANOIR, _Dec 2018 – Mai 2019_
* Mise en oeuvre d’une stratégie de mesure d’énergie sans contact avec un détecteur à inductance cinétique.
* **Contact**, Dr Julien Billard et Dr Alexandre Juillard

#### **Institut de Physique Nucléaire de Lyon**
* **Stagiaire**, groupe MANOIR, _Avr 2018 – Juil 2018_
* Etudes des détecteurs de particules par interactions phonons/paires de Cooper pour une application en recherche
d’évènements rares (neutrinos, WIMP,...)
* **Contacts**, Dr Julien Billard et Dr Alexandre Juillard

#### **Laboratoire de Physique - ENS de Lyon**
* **Stagiaire**, équipes 'Signaux et Systèmes' et 'Géophysique',  _Avr 2017 – Juil 2017_
* Caractérisation des régimes dynamiques dans le cadre de la friction solide/solide en utilisant des méthodes
non-linéaires de traitement du signal.
* **Contacts**, Dr Nelly Pustelnik et Dr Valérie Vidal

#### **Institut de Physique Nucléaire de Lyon**
* **Stagiaire**, groupe Interactions Particules Matière, _Mai 2016 – Juil 2016_
* Développement et contrôle de l’électronique d’acquisition de DIAM (Dispositif d’Irradiation d’Agrégats
Moléculaires).
* **Contacts**, Dr Hassan Abdoul-Carime et Pr Michel Farizon

***
### Research works

#### Publications (peer-reviewed)
1. J. Goupy, J. Colas, M. Calvo, J. Billard, P. Camus, R. Germond, A. Juillard, L. Vagneron, M. De
Jesus, F. Levy-Bertrand, A. Monfardini, ”Contact-less phonon detection with massive cryogenic
absorbers”, **Appl. Phys. Lett.** vol. 115,no. 22,pp. 223506 , Juin 2019.

2. Jules Colas, Nelly Pustelnik, Cristobal Oliver, Patrice Abry, Jean-Christophe Géminard, and Valérie
Vidal, “Nonlinear denoising for characterization of solid friction under low confinement pressure,”
**Phys. Rev. E**,vol. 100,no. 3,pp. 032803 ,Sep 2019.
#### Conferences (speaker)
1. Jules Colas , “Development of Kinetic Inductance Detectors with contact-less feedline
as heat sensors for rare events experiments,” in  **18th International Workshop on Low Temperature Detectors** , Milano, Italia, Juil 2019.

***

### Compétences

#### Langues
* **Français** : Langue maternelle.
* **Anglais** : Professionnel (écrit, oral | TOEFL ITP : 593 / 677).  

#### Informatique
* Python, bash, C/C++, LaTex, ...
* digital signal processing
* SolidWorks, Catia, AlphaCam, Ansys, LabView ...

#### Electronique
* linear circuit
* sensors / acquisition

***
